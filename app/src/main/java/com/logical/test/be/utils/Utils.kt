package com.logical.test.be.utils

import android.content.Context
import android.os.Build
import android.preference.PreferenceManager
import android.text.Html
import android.text.Spanned
import com.logical.test.be.ui.test.TestActivity
import com.scottyab.aescrypt.AESCrypt
import java.security.GeneralSecurityException

object Utils {

    lateinit var key: String

    fun initKey(context: Context) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val k = sharedPreferences.getString("app_id", "")!! + TestActivity.key
        key = ""
        try {
            key = AESCrypt.decrypt("1a462932f3f09150083c1ad0dcc57fb7", k)
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }
    }

}
