package com.logical.test.be.ui.test


import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.logical.test.be.R
import com.logical.test.be.data.Category
import com.logical.test.be.data.ResultTest
import com.logical.test.be.data.Test
import com.logical.test.be.ui.test.result.ResultTestFragment
import com.logical.test.be.utils.PreferenceUtils
import com.minhphu.basemodule.ads.AdUtil
import com.minhphu.basemodule.extensions.OnClickView
import com.minhphu.basemodule.extensions.setOnClick
import com.minhphu.basemodule.extensions.setTextHtml
import com.vicpin.krealmextensions.save
import kotlinx.android.synthetic.main.fragment_test.*
import java.util.*
import kotlin.collections.ArrayList

class TestFragment : androidx.fragment.app.Fragment(), OnClickView {

    companion object {
        fun newInstance(catId: Int): TestFragment {
            val testFragment = TestFragment()
            testFragment.catId = catId
            val testList: List<Test> = Test.getAllByCat(catId)
            testList.forEach { test -> test.userAns = "" }
            testFragment.testList = testList.shuffled().take(10)
            return testFragment
        }
        var testId = 0
    }

    private var catId: Int = 0
    lateinit var testList: List<Test>
    var resultTestList: ArrayList<ResultTest> = ArrayList()
    private var positionTestList = 0
    private var isClickable = true

    private var handlerShowResult: Handler = Handler()
    private var handlerNextQuestion: Handler = Handler()
    private var runnableShowResult: Runnable? = null
    private var runnableNextQuestion: Runnable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater.inflate(R.layout.fragment_test, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClick(listOf(btnA, btnB, btnC, btnD), this)
        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.title = Category.getCatById(catId)!!.name

        if (positionTestList < testList.size) {
            nextQuestion(testList[positionTestList])
            try {
                if (adsTestContainer.childCount == 0)
                    if (!PreferenceUtils.checkPurchaseAd(requireContext())) {
                        AdUtil.showNativeAdFbSmall(activity!!, adsTestContainer, getString(R.string.adsFb_TestFragment),getString(R.string.adsGg_TestFragment))
                    }
            } catch (e: Exception) {
                Log.e("BE", e.message)
            }
        } else {
            showTestResult()
        }
    }

    private fun resetButtonState() {
        (btnA as androidx.cardview.widget.CardView).setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.white))
        (btnB as androidx.cardview.widget.CardView).setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.white))
        (btnC as androidx.cardview.widget.CardView).setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.white))
        (btnD as androidx.cardview.widget.CardView).setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.white))
    }

    private fun nextQuestion(test: Test) {
        isClickable = true
        resetButtonState()
        showTest(test)
    }

    private fun showTest(test: Test) {
        testId = test.id.toInt()
        txtQuesNumber?.text = getString(R.string.txt_progress, positionTestList + 1, testList.size)
        val question = test.question().replace("\\n ", System.getProperty("line.separator"))
        txtQuestion?.setTextHtml(question)

        val testAnsList = test.getRandomAns()
        txtA?.text = testAnsList[0]
        txtB?.text = testAnsList[1]
        txtC?.text = testAnsList[2]
        if (testAnsList.size > 3)
            txtD?.text = testAnsList[3]
        else
            btnD?.visibility = View.GONE
    }

    private fun showTestResult() {
        resultTestList.sortWith(Comparator { x, y -> x.getAnsResult().compareTo(y.getAnsResult()) })
        val transaction = fragmentManager?.beginTransaction()
        val resultFragment = ResultTestFragment.newInstance(resultTestList, catId)
        transaction?.replace(R.id.container, resultFragment)
        transaction?.commit()
    }


    private fun removeHandler() {
        if (runnableShowResult != null) {
            handlerShowResult.removeCallbacks(runnableShowResult)
        }
        if (runnableNextQuestion != null) {
            handlerNextQuestion.removeCallbacks(runnableNextQuestion)
        }
    }

    private fun calScore(): Int {
        return resultTestList.count { it.userAns == it.ans }
    }

    override fun onPause() {
        removeHandler()
        if (resultTestList.size != 0) {
            val category = Category.getCatById(catId)
            if (calScore() > category?.score!!) {
                category.score = calScore().toLong()
                category.save()
            }
        }
        super.onPause()
    }

    override fun eventClick(v: View) {
        if (isClickable) {
            isClickable = false

            val test = testList[positionTestList]

            val resultTest = ResultTest()
            resultTest.question = test.question()
            resultTest.ans = test.getAnswers()
            resultTest.userAns = ((v as ViewGroup).getChildAt(0) as TextView).text.toString()
            resultTestList.add(resultTest)

            if (resultTest.getAnsResult()) {
                (v as androidx.cardview.widget.CardView).setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.green))
            } else {
                (v as androidx.cardview.widget.CardView).setCardBackgroundColor(ContextCompat.getColor(activity!!, R.color.red))
            }

            positionTestList++

            if (positionTestList == testList.size) {
                runnableShowResult = Runnable {
                    showTestResult()
                }
                handlerShowResult.postDelayed(runnableShowResult, 2000)

            } else {
                runnableNextQuestion = Runnable {
                    nextQuestion(testList[positionTestList])
                }
                handlerNextQuestion.postDelayed(runnableNextQuestion, 2000)
            }
        }
    }

}

