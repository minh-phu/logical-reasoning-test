package com.logical.test.be.ui.test.result

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.logical.test.be.R
import com.logical.test.be.data.ResultTest
import kotlinx.android.synthetic.main.fragment_result.*

class ResultTestFragment : androidx.fragment.app.Fragment() {
    private var resultTestList: List<ResultTest>? = null
    private var catId: Int = 0


    companion object {
        fun newInstance(resultTestList: List<ResultTest>, catId: Int): ResultTestFragment {
            val resultFragment = ResultTestFragment()
            resultFragment.resultTestList = resultTestList
            resultFragment.catId = catId
            return resultFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.fragment_result, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val resultAdapter = ResultTestAdapter(activity!!, resultTestList!!, true)
        recycler_view.layoutManager = LinearLayoutManager(activity)
        recycler_view.adapter = resultAdapter


        btnHome.setOnClickListener { activity!!.finish() }
    }

}

