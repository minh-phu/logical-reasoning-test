package com.logical.test.be.utils

import android.content.Context
import android.preference.PreferenceManager
import com.logical.test.be.MyApplication

object PreferenceUtils {
    private const val HIDE_AD = "hide_ad"
    private const val IS_RATED = "isRate"

    fun checkPurchaseAd(context: Context): Boolean {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getBoolean(HIDE_AD, false)
    }

    fun updatePurchaseAd(context: Context,isPurchase:Boolean){
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putBoolean(HIDE_AD, isPurchase).apply()
    }

    fun updateIsRated(context: Context){
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        prefs.edit().putBoolean(IS_RATED, true).apply()
    }

    fun checkUserRateApp(): Boolean {
        return MyApplication.sharedPreferences.getBoolean(IS_RATED, false)
    }
}