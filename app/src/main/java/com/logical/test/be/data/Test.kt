package com.logical.test.be.data

import com.minhphu.basemodule.utils.CipherHelper
import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Test : RealmObject() {

    @PrimaryKey
    var id: Long = 0
    var catId: Long? = null
    var question: String? = null
    private var optionA: String? = null
    private var optionB: String? = null
    private var optionC: String? = null
    private var optionD: String? = null
    private var ans: String? = null

    var userAns: String? = null
    var hint: String? = null

    fun question(): String {
        val decQues = CipherHelper.decrypt(question!!)
        val re = Regex("</*p>")
        return re.replace(decQues, "")
    }

    private fun optionA(): String {
        return optionA!!.replace("__", "'")
    }

    private fun optionB(): String {
        return optionB!!.replace("__", "'")
    }

    private fun optionC(): String {
        return optionC!!.replace("__", "'")
    }

    private fun optionD(): String {
        return optionD!!.replace("__", "'")
    }

    fun getAnswers(): String {
        when (ans) {
            "A" -> return optionA()
            "B" -> return optionB()
            "C" -> return optionC()
            "D" -> return optionD()
        }
        return ""
    }


    fun getRandomAns(): ArrayList<String> {
        val randList = ArrayList<String>(arrayListOf(optionA(), optionB(), optionC()))
        if (optionD !=null && optionD?.length!! > 0) {
            randList.add(optionD())
        }
        randList.shuffle()
        return randList
    }

    companion object {

        fun getById(cat_id: Int): Test? {
            return Test().queryFirst { equalTo("id", cat_id) }
        }

        fun getAllByCat(catId: Int): List<Test> {
            return Test().query { equalTo("catId", catId) }
        }

    }

}