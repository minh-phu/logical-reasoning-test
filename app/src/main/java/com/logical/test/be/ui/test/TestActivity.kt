package com.logical.test.be.ui.test

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.logical.test.be.R
import kotlinx.android.synthetic.main.activity_test.*

class TestActivity : AppCompatActivity() {

    private var testFragment: TestFragment? = null
    private var catId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        setSupportActionBar(toolbarPracticeTest)
        catId = intent.getIntExtra("catId", 0)
        testFragment = TestFragment.newInstance(catId)
        supportFragmentManager.beginTransaction().add(R.id.container, testFragment!!).commit()
    }


    companion object {
        var key = "VNbpHff6xlFtXQ"
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_test, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.question_list -> showResultUncompletedTest()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showResultUncompletedTest() {
        if (testFragment?.isVisible!!) {
            val newFragment = HintFragment.newInstance(TestFragment.testId)
            supportFragmentManager.beginTransaction().replace(R.id.container, newFragment).addToBackStack("").commit()
        }
    }
}