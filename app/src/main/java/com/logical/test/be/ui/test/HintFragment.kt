package com.logical.test.be.ui.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.logical.test.be.utils.Utils
import com.logical.test.be.R
import com.logical.test.be.data.Test
import com.minhphu.basemodule.extensions.setTextHtml
import kotlinx.android.synthetic.main.fragment_hint.*


class HintFragment : androidx.fragment.app.Fragment() {
    private var catId: Int = 0

    companion object {
        fun newInstance(catId: Int): HintFragment {
            val contentFragment = HintFragment()
            contentFragment.catId = catId
            return contentFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater.inflate(R.layout.fragment_hint, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val test = Test.getById(catId)!!
        txtTitle.setTextHtml(test.question())
        txtContent.setTextHtml(test.hint!!)
    }
}