package com.logical.test.be.ui.test.result

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.logical.test.be.R
import com.logical.test.be.data.ResultTest
import com.logical.test.be.utils.PreferenceUtils
import com.minhphu.basemodule.ads.AdUtil
import com.minhphu.basemodule.extensions.setTextHtml
import kotlinx.android.synthetic.main.item_result.view.*
import kotlinx.android.synthetic.main.item_result_header.view.*


class ResultTestAdapter(private val context: Context, private val resultTestList: List<ResultTest>, private val isResult: Boolean) : RecyclerView.Adapter<ResultTestAdapter.ViewHolder>() {
    companion object {
        const val PASSED_SCORE = 7
        const val ITEM_SHOW_ADS = 8
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == 0) {
            val header = LayoutInflater.from(context).inflate(R.layout.item_result_header, viewGroup, false)
            val layoutNativeAds = header.findViewById<androidx.cardview.widget.CardView>(R.id.resultAdsContainerTop)
            if (isResult) {
                if (!PreferenceUtils.checkPurchaseAd(context)) {
                    if (layoutNativeAds.childCount == 0) {
                        AdUtil.showNativeAdsFbSmallInList(context, layoutNativeAds, context.getString(R.string.adsFb_ResultTestFragmentTop), context.getString(R.string.adsGg_ResultTestFragmentTop))
                    }
                }
                val headerResult = header.findViewById<LinearLayout>(R.id.headerResult)
                headerResult.visibility = View.VISIBLE
            } else {
                if (!PreferenceUtils.checkPurchaseAd(context)) {
                    if (layoutNativeAds.childCount == 0) {
                        AdUtil.showNativeAdsFbSmallInList(context, layoutNativeAds, context.getString(R.string.adsFb_UnResultTestFragmentTop), context.getString(R.string.adsGg_ResultTestFragmentTop))
                    }
                }
            }
            when {
                score >= (resultTestList.size * PASSED_SCORE / 10) -> when (score) {
                    resultTestList.size -> header.txtResult.text = context.getString(R.string.perfect)
                    else -> header.txtResult.text = context.getString(R.string.passed)
                }
                else -> header.txtResult.text = context.getString(R.string.failed)
            }
            header.txtProgress.text = context.getString(R.string.txt_progress, score, resultTestList.size)
            header.resultProgress.progress = score
            header.resultProgress.max = resultTestList.size
            return ViewHolder(header)
        }

        val view = LayoutInflater.from(context).inflate(R.layout.item_result, viewGroup, false)
        return ViewHolder(view)
    }

    private val score = calScore()

    override fun onBindViewHolder(itemViewHolder: ViewHolder, position: Int) {
        val view = itemViewHolder.itemView
        if (position > 0) {
            // skip 1 position for header
            val finalPosition = position - 1
            val resultTest = resultTestList[finalPosition]
            if (resultTest.userAns!!.isEmpty()) {
                view.visibility = View.GONE
            } else {
                view.txtQuestion.setTextHtml(resultTest.question!!.replace("\\n ", System.getProperty("line.separator")))
                view.txtUserAns.text = context.getString(R.string.your_choose, resultTest.userAns)
                view.txtCorrectAns.text = context.getString(R.string.correct_ans, resultTest.ans)

                if (position == resultTestList.size && position >= ITEM_SHOW_ADS) {
                    val layoutNativeAds = view.findViewById<androidx.cardview.widget.CardView>(R.id.resultAdsContainerBottom)
                    if(!PreferenceUtils.checkPurchaseAd(context)) {
                        if (isResult)
                            AdUtil.showNativeAdFbLarge(context, layoutNativeAds, R.string.adsFb_ResultTestFragmentBottom, context.getString(R.string.adsGg_ResultTestFragmentBottom))
                        else
                            AdUtil.showNativeAdFbLarge(context, layoutNativeAds, R.string.adsFb_UnResultTestFragmentBottom, context.getString(R.string.adsGg_ResultTestFragmentBottom))
                    }
                }

                if (resultTest.ans != resultTest.userAns) {
                    itemViewHolder.itemView.txtUserAns.setTextColor(ContextCompat.getColor(context, R.color.red))
                    if (position == 1) {
                        itemViewHolder.itemView.txtHeader.text = context.getString(R.string.wrong_ans)
                        itemViewHolder.itemView.txtHeader.visibility = View.VISIBLE
                    }
                } else {
                    view.txtCorrectAns.visibility = View.GONE
                    if (position == (resultTestList.size - score + 1)) {
                        itemViewHolder.itemView.txtHeader.text = context.getString(R.string.right_ans)
                        itemViewHolder.itemView.txtHeader.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int = position


    private fun calScore(): Int {
        return resultTestList.count { it.userAns == it.ans }
    }

    // plus 1 because result have header
    override fun getItemCount(): Int = resultTestList.count { !it.userAns!!.isEmpty() } + 1

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
